# woodpecker-xmpp

[![status-badge](https://ci.polynom.me/api/badges/40/status.svg)](https://ci.polynom.me/repos/40)

A simple plugin for [Woodpecker CI](https://woodpecker-ci.org/) that allows sending notifications
about the pipeline status using XMPP (direct messages or MUC messages).

The plugin is just a small shell script wrapping [go-sendxmpp](https://salsa.debian.org/mdosch/go-sendxmpp).

## Config

In order for the plugin to work, the following secrets must be set:

| Secret | Description |
| --- | --- |
| `xmpp_jid` | The JID to authenticate as |
| `xmpp_password` | The password to authenticate with |

If your server has special connection parameters (hostname:port), then you can also
specify the `xmpp_server` secret to something like `example.org:443`.

Plaintext configuration options for the plugin are:

| Setting | Description ]
| --- | --- |
| `xmpp_tls` | If set, enables direct TLS |
| `xmpp_is_muc` | If set, tells go-sendxmpp that the message is to be sent to a groupchat |
| `xmpp_recipient` | The recipient (XMPP account or MUC) of the message |
| `xmpp_alias` | The nickname to use when joining the MUC. Has no effect if `xmpp_is_muc` is not set |

## Testing

Build the image using docker or podman as `papatutuwawa/woodpecker-xmpp` and run

```
docker run --rm \
  -e CI_BUILD_STATUS=success \
  -e CI_COMMIT_HASH=aaaaaaaaa \
  -e CI_REPO=example-user/repo \
  -e XMPP_JID=user@example.org \
  -e XMPP_PASSWORD=s3cr3t-p4ssw0rd \
  -e PLUGIN_XMPP_RECIPIENT=other-jid@example.org \
  papatutuwawa/woodpecker-xmpp
```

## License

See `./LICENSE`.
