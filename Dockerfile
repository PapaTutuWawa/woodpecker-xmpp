FROM alpine:3.18.3

# We need bash for parameter arrays
RUN apk add --no-cache go-sendxmpp bash

# Include the main script
ADD --chmod=555 entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
