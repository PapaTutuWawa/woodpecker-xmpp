#!/bin/bash
# Allow debugging
[[ -n "${PLUGIN_DEBUG}" ]] && set -x

# Set the correct message
if [[ "${CI_BUILD_STATUS}" = "success" ]]; then
  MESSAGE="✔️  Pipeline for ${CI_COMMIT_SHA} on ${CI_REPO} succeeded."
else
  MESSAGE="❌ Pipeline for ${CI_COMMIT_SHA} on ${CI_REPO} failed on ${CI_MACHINE} (${CI_SYSTEM_ARCH}).\nSee ${CI_BUILD_LINK}"
fi

# Tell go-sendxmpp about the server to connect to, if specified
SERVER_DETAILS=""
if [[ -n "${XMPP_SERVER}" ]]; then
  SERVER_DETAILS=(-j "${XMPP_SERVER}")
fi

# Force TLS, if configured
SERVER_TLS=""
if [[ -n "${PLUGIN_XMPP_TLS}" ]]; then
  SERVER_TLS=--tls
fi

# Enable groupchat settings, if configured.
GROUPCHAT=""
if [[ -n "${PLUGIN_XMPP_IS_MUC}" ]]; then
  GROUPCHAT="-c"

  # Make the bot nickname configurable
  BOT_ALIAS=(-a "${PLUGIN_XMPP_ALIAS:-"Woodpecker CI"}")
fi

# Sanity checks
CANCEL=""
if [[ -z "${XMPP_JID}" ]]; then
  echo "Error: No JID specified using XMPP_JID"
  CANCEL="y"
fi
if [[ -z "${XMPP_PASSWORD}" ]]; then
  echo "Error: No password specified using XMPP_PASSWORD"
  CANCEL="y"
fi
[[ -n "${CANCEL}" ]] && exit 1

# "Post" the message
# Disable SC2086 (double quote to prevent word splitting) and SC2068 (double quote array expansion to prevent word splitting)
# because we DO want word splitting here.
# shellcheck disable=SC2086,SC2068
echo -e "$MESSAGE" | go-sendxmpp -u ${XMPP_JID} -p ${XMPP_PASSWORD} ${SERVER_DETAILS[@]} ${SERVER_TLS} ${BOT_ALIAS[@]} ${GROUPCHAT} ${PLUGIN_XMPP_RECIPIENT}
